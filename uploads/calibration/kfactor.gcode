; ### Marlin K-Factor Calibration Pattern ###
; -------------------------------------------
;
; Created: Tue Jan 29 2019 22:21:08 GMT-0600 (Central Standard Time)
;
; Settings Printer:
; Filament Diameter = 1.75 mm
; Nozzle Diameter = 0.4 mm
; Nozzle Temperature = 212 °C
; Bed Temperature = 65 °C
; Retraction Distance = 4 mm
; Layer Height = 0.2 mm
; Z-axis Offset = 0 mm
;
; Settings Print Bed:
; Bed Shape = Rect
; Bed Size X = 235 mm
; Bed Size Y = 235 mm
; Origin Bed Center = false
;
; Settings Speed:
; Slow Printing Speed = 1200 mm/min
; Fast Printing Speed = 3600 mm/min
; Movement Speed = 7800 mm/min
; Retract Speed = 1500 mm/min
; Printing Acceleration = 1500 mm/s^2
; Jerk X-axis =  firmware default
; Jerk Y-axis =  firmware default
; Jerk Z-axis =  firmware default
; Jerk Extruder =  firmware default
;
; Settings Pattern:
; Linear Advance Version = 1.5
; Starting Value Factor = 0
; Ending Value Factor = 2.4
; Factor Stepping = 0.2
; Test Line Spacing = 5 mm
; Test Line Length Slow = 20 mm
; Test Line Length Fast = 40 mm
; Print Pattern = Standard
; Print Frame = false
; Number Lines = true
; Print Size X = 98 mm
; Print Size Y = 85 mm
; Print Rotation = 0 degree
;
; Settings Advance:
; Nozzle / Line Ratio = 1.1
; Bed leveling = M420 L0 S1;
; Use FWRETRACT = false
; Extrusion Multiplier = 1
; Prime Nozzle = true
; Prime Extrusion Multiplier = 2.5
; Prime Speed = 1800
; Dwell Time = 2 s
;
; prepare printing
;
M104 S212 ; set nozzle temperature but do not wait
M190 S65 ; set bed temperature and wait
M109 S212 ; block waiting for nozzle temp
G28 ; home all axes with heated bed
M420 L0 S1;; Activate bed leveling compensation
G21 ; set units to millimeters
M204 P1500 ; set acceleration
G90 ; use absolute coordinates
M83 ; use relative distances for extrusion
G92 E0 ; reset extruder distance
G1 X117.5 Y117.5 F7800 ; move to start
G1 Z0.2 F1200 ; move to layer height
;
; prime nozzle
;
G1 X68.5 Y75 F7800 ; move to start
G1 X68.5 Y160 E7.7746 F1800 ; print line
G1 X69.16 Y160 F7800 ; move to start
G1 X69.16 Y75 E7.7746 F1800 ; print line
G1 E-4 F1500 ; retract
;
; start the Test pattern
;
G4 P2000 ; Pause (dwell) for 2 seconds
G1 X78.5 Y75 F7800 ; move to start
M900 K0 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y75 E0.7317 F1200 ; print line
G1 X138.5 Y75 E1.4634 F3600 ; print line
G1 X158.5 Y75 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y80 F7800 ; move to start
M900 K0.2 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y80 E0.7317 F1200 ; print line
G1 X138.5 Y80 E1.4634 F3600 ; print line
G1 X158.5 Y80 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y85 F7800 ; move to start
M900 K0.4 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y85 E0.7317 F1200 ; print line
G1 X138.5 Y85 E1.4634 F3600 ; print line
G1 X158.5 Y85 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y90 F7800 ; move to start
M900 K0.6 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y90 E0.7317 F1200 ; print line
G1 X138.5 Y90 E1.4634 F3600 ; print line
G1 X158.5 Y90 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y95 F7800 ; move to start
M900 K0.8 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y95 E0.7317 F1200 ; print line
G1 X138.5 Y95 E1.4634 F3600 ; print line
G1 X158.5 Y95 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y100 F7800 ; move to start
M900 K1 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y100 E0.7317 F1200 ; print line
G1 X138.5 Y100 E1.4634 F3600 ; print line
G1 X158.5 Y100 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y105 F7800 ; move to start
M900 K1.2 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y105 E0.7317 F1200 ; print line
G1 X138.5 Y105 E1.4634 F3600 ; print line
G1 X158.5 Y105 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y110 F7800 ; move to start
M900 K1.4 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y110 E0.7317 F1200 ; print line
G1 X138.5 Y110 E1.4634 F3600 ; print line
G1 X158.5 Y110 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y115 F7800 ; move to start
M900 K1.6 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y115 E0.7317 F1200 ; print line
G1 X138.5 Y115 E1.4634 F3600 ; print line
G1 X158.5 Y115 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y120 F7800 ; move to start
M900 K1.8 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y120 E0.7317 F1200 ; print line
G1 X138.5 Y120 E1.4634 F3600 ; print line
G1 X158.5 Y120 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y125 F7800 ; move to start
M900 K2 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y125 E0.7317 F1200 ; print line
G1 X138.5 Y125 E1.4634 F3600 ; print line
G1 X158.5 Y125 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y130 F7800 ; move to start
M900 K2.2 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y130 E0.7317 F1200 ; print line
G1 X138.5 Y130 E1.4634 F3600 ; print line
G1 X158.5 Y130 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X78.5 Y135 F7800 ; move to start
M900 K2.4 ; set K-factor
G1 E4 F1500 ; un-retract
G1 X98.5 Y135 E0.7317 F1200 ; print line
G1 X138.5 Y135 E1.4634 F3600 ; print line
G1 X158.5 Y135 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
;
; mark the test area for reference
;
M900 K0 ; set K-factor 0
G1 X98.5 Y140 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X98.5 Y160 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 X138.5 Y140 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X138.5 Y160 E0.7317 F1200 ; print line
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
;
; print K-values
;
G1 X160.5 Y73 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X162.5 Y73 E0.0732 F1200 ; 0
G1 X162.5 Y75 E0.0732 F1200 ; 0
G1 X162.5 Y77 E0.0732 F1200 ; 0
G1 X160.5 Y77 E0.0732 F1200 ; 0
G1 X160.5 Y75 E0.0732 F1200 ; 0
G1 X160.5 Y73 E0.0732 F1200 ; 0
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y83 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X162.5 Y83 E0.0732 F1200 ; 0
G1 X162.5 Y85 E0.0732 F1200 ; 0
G1 X162.5 Y87 E0.0732 F1200 ; 0
G1 X160.5 Y87 E0.0732 F1200 ; 0
G1 X160.5 Y85 E0.0732 F1200 ; 0
G1 X160.5 Y83 E0.0732 F1200 ; 0
G1 E-4 F1500 ; retract
G1 X163.5 Y83 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X163.5 Y83.4 E0.0146 F1200 ; dot
G1 E-4 F1500 ; retract
G1 X164.5 Y83 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X164.5 Y85 F7800 ; move to start
G1 X164.5 Y87 F7800 ; move to start
G1 X164.5 Y85 E0.0732 F1200 ; 4
G1 X166.5 Y85 E0.0732 F1200 ; 4
G1 X166.5 Y87 F7800 ; move to start
G1 X166.5 Y85 E0.0732 F1200 ; 4
G1 X166.5 Y83 E0.0732 F1200 ; 4
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y93 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X162.5 Y93 E0.0732 F1200 ; 0
G1 X162.5 Y95 E0.0732 F1200 ; 0
G1 X162.5 Y97 E0.0732 F1200 ; 0
G1 X160.5 Y97 E0.0732 F1200 ; 0
G1 X160.5 Y95 E0.0732 F1200 ; 0
G1 X160.5 Y93 E0.0732 F1200 ; 0
G1 E-4 F1500 ; retract
G1 X163.5 Y93 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X163.5 Y93.4 E0.0146 F1200 ; dot
G1 E-4 F1500 ; retract
G1 X164.5 Y93 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X164.5 Y95 F7800 ; move to start
G1 X166.5 Y95 E0.0732 F1200 ; 8
G1 X166.5 Y93 E0.0732 F1200 ; 8
G1 X164.5 Y93 E0.0732 F1200 ; 8
G1 X164.5 Y95 E0.0732 F1200 ; 8
G1 X164.5 Y97 E0.0732 F1200 ; 8
G1 X166.5 Y97 E0.0732 F1200 ; 8
G1 X166.5 Y95 E0.0732 F1200 ; 8
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y103 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X160.5 Y105 E0.0732 F1200 ; 1
G1 X160.5 Y107 E0.0732 F1200 ; 1
G1 E-4 F1500 ; retract
G1 X161.5 Y103 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X161.5 Y103.4 E0.0146 F1200 ; dot
G1 E-4 F1500 ; retract
G1 X162.5 Y103 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X162.5 Y105 F7800 ; move to start
G1 X162.5 Y107 F7800 ; move to start
G1 X164.5 Y107 E0.0732 F1200 ; 2
G1 X164.5 Y105 E0.0732 F1200 ; 2
G1 X162.5 Y105 E0.0732 F1200 ; 2
G1 X162.5 Y103 E0.0732 F1200 ; 2
G1 X164.5 Y103 E0.0732 F1200 ; 2
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y113 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X160.5 Y115 E0.0732 F1200 ; 1
G1 X160.5 Y117 E0.0732 F1200 ; 1
G1 E-4 F1500 ; retract
G1 X161.5 Y113 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X161.5 Y113.4 E0.0146 F1200 ; dot
G1 E-4 F1500 ; retract
G1 X162.5 Y113 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X162.5 Y115 F7800 ; move to start
G1 X164.5 Y115 E0.0732 F1200 ; 6
G1 X164.5 Y113 E0.0732 F1200 ; 6
G1 X162.5 Y113 E0.0732 F1200 ; 6
G1 X162.5 Y115 E0.0732 F1200 ; 6
G1 X162.5 Y117 E0.0732 F1200 ; 6
G1 X164.5 Y117 E0.0732 F1200 ; 6
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y123 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X160.5 Y125 F7800 ; move to start
G1 X160.5 Y127 F7800 ; move to start
G1 X162.5 Y127 E0.0732 F1200 ; 2
G1 X162.5 Y125 E0.0732 F1200 ; 2
G1 X160.5 Y125 E0.0732 F1200 ; 2
G1 X160.5 Y123 E0.0732 F1200 ; 2
G1 X162.5 Y123 E0.0732 F1200 ; 2
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
G1 X160.5 Y133 F7800 ; move to start
G1 Z0.2 F1200 ; zHop
G1 E4 F1500 ; un-retract
G1 X160.5 Y135 F7800 ; move to start
G1 X160.5 Y137 F7800 ; move to start
G1 X162.5 Y137 E0.0732 F1200 ; 2
G1 X162.5 Y135 E0.0732 F1200 ; 2
G1 X160.5 Y135 E0.0732 F1200 ; 2
G1 X160.5 Y133 E0.0732 F1200 ; 2
G1 X162.5 Y133 E0.0732 F1200 ; 2
G1 E-4 F1500 ; retract
G1 X163.5 Y133 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X163.5 Y133.4 E0.0146 F1200 ; dot
G1 E-4 F1500 ; retract
G1 X164.5 Y133 F7800 ; move to start
G1 E4 F1500 ; un-retract
G1 X164.5 Y135 F7800 ; move to start
G1 X164.5 Y137 F7800 ; move to start
G1 X164.5 Y135 E0.0732 F1200 ; 4
G1 X166.5 Y135 E0.0732 F1200 ; 4
G1 X166.5 Y137 F7800 ; move to start
G1 X166.5 Y135 E0.0732 F1200 ; 4
G1 X166.5 Y133 E0.0732 F1200 ; 4
G1 E-4 F1500 ; retract
G1 Z0.3 F1200 ; zHop
;
; finish
;
M104 S0 ; turn off hotend
M140 S0 ; turn off bed
G1 Z30 X235 Y235 F7800 ; move away from the print
M84 ; disable motors
M502 ; resets parameters from ROM
M501 ; resets parameters from EEPROM
;