axes:
  e:
    inverted: false
    speed: 6000
  x:
    inverted: false
    speed: 60000
  y:
    inverted: false
    speed: 60000
  z:
    inverted: false
    speed: 1800
color: default
extruder:
  count: 1
  nozzleDiameter: 0.4
  offsets:
  - - 0.0
    - 0.0
  sharedNozzle: false
heatedBed: true
id: Ender3
model: Ender3
name: Ender3
volume:
  custom_box: false
  depth: 230.0
  formFactor: rectangular
  height: 230.0
  origin: lowerleft
  width: 230.0
